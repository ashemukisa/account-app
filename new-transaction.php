<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add New Transaction</title>
    <style>
        #submit-transaction {
            margin-top: 10px;
            height: 40px;
            font-size: 16px;
            color: white;
            background-color: blue;
        }
        #transaction-div{
            border: 1px red solid;
            display: inline-block;
            background-color: lightcyan;
            padding: 20px;
        }
    </style>
</head>

<body>
    <form action="" method="post">
    <p>Please Enter the amount and description below:</p>   
        <div id="transaction-div">
            
            <div style="margin-bottom: 10px;">
                <label for="transaction-amount">Transaction Amount:</label>
                <input id="transaction-amount" name="transaction-amount" type="number">
            </div>

            <div>
                <label for="transaction-amount" style="margin-right: 70px;">Description:</label>
                <input id="transaction-amount" name="transaction-amount" type="text">
            </div>
            <div>
                <input id="submit-transaction" type="submit" value="Add Transaction">
            </div>
        </div>
    </form>

</body>

</html>